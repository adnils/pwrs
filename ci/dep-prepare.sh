#!/bin/bash

# Add erlang-solutions
wget http://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb && sudo dpkg -i erlang-solutions_1.0_all.deb
sudo apt-get update
# Install Elixir
sudo apt-get install -y elixir
# Install package tools
if [ ! -e $HOME/.mix/rebar ]; then
  yes Y | LC_ALL=en_GB.UTF-8 mix local.hex
  yes Y | LC_ALL=en_GB.UTF-8 mix local.rebar
fi
# Install Phoenix
if [ ! -e $HOME/.mix/archives/phoenix_new-0.13.1.ez ]; then
  yes Y | LC_ALL=en_GB.UTF-8 mix archive.install https://github.com/phoenixframework/phoenix/releases/download/v0.13.1/phoenix_new-0.13.1.ez
fi
# Compile app
(cd ../ && yes Y | mix do deps.get, deps.compile, compile)

exit 0
