#!/bin/bash

export PATH="$HOME/dependencies/erlang/bin:$HOME/dependencies/elixir/bin:$PATH"
export MIX_ENV="test"
(cd $HOME/$CIRCLE_PROJECT_REPONAME && mix test)
