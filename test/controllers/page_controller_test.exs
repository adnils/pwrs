defmodule Pwrs.PageControllerTest do
  use Pwrs.ConnCase

  test "GET /" do
    conn = get conn(), "/"
    assert html_response(conn, 200) =~ "Welcome to Pwrs!"
  end
end
