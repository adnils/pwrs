defmodule Pwrs.PageController do
  use Pwrs.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
