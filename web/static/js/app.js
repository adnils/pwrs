import React from 'react'
import Router from 'react-router'
let { Route, RouteHandler, Link, DefaultRoute } = Router;

import Home from './components/Home.js'
import About from './components/About.js'

export default class App extends React.Component {

  render () {
    return (
      <div>
        <ul>
          <li><Link to="home">Home</Link></li>
          <li><Link to="about">About</Link></li>
        </ul>
        <RouteHandler/>
      </div>
    )
  }
}

var routes = (
  <Route handler={App} path="/">
    <DefaultRoute handler={Home} />
    <Route name="home" handler={Home} />
    <Route name="about" handler={About} />
  </Route>
);

Router.run(routes, Router.HistoryLocation, function (Handler) {
  React.render(<Handler/>, document.getElementById('app'))
})
