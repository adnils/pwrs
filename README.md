# Phoenix Webpack React Starter
[![Circle CI](https://circleci.com/gh/adnils/pwrs.svg?style=svg)](https://circleci.com/gh/adnils/pwrs)

A starter kit for building web applications in Elixir and JavaScript with Phoenix and React, using webpack for static asset compilation (Phoenix uses brunch.io by default).

Comes with a very fast configuration to do testing with CircleCI.

## Application stack
- [Elixir](http://elixir-lang.org)
- [Phoenix](http://phoenixframework.org)
- [React](http://facebook.github.io/react/)
- [Webpack](http://webpack.github.io/) for static asset compilation.
- [Babel](http://babeljs.io/) for the best ES6 support.

## Continuous Integration
- CircleCI

## Usage

### Local Installation
[Install Elixir](http://elixir-lang.org/install.html)

[Install Phoenix](http://www.phoenixframework.org/v0.13.1/docs/up-and-running)
```
git clone https://github.com/adnils/pwrs.git && cd pwrs
npm install
mix deps.get
```

### Installation with Vagrant
This will take a while the **first** time as it compiles Erlang and Elixir from source
```
git clone https://github.com/adnils/pwrs.git && cd pwrs
vagrant up
vagrant ssh
cd /vagrant
```

### Run development server
```
mix phoenix.server
```


## TODO
- [x] Scripts to install CI dependencies
- [x] Add React
- [x] Add basic react-router
- [x] Add Vagrant setup
- [ ] Add react-hot-loader
